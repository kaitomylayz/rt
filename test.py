import datetime
from datetime import date

# Создание объекта даты
today = datetime.date.today()
print(f"Сегодняшняя дата: {today}")

# Создание объекта времени
now = datetime.datetime.now()
print(f"Текущее время: {now}")

# Вычисление разницы между датами
other_date = datetime.date(2023, 11, 3)
date_difference = other_date - today
print(f"Разница между датами: {date_difference.days} дней")

# Форматирование даты и времени
date_str = date(2023, 5, 22)
formatted_date = date_str.strftime('%d/%m/%Y')

# Добавление времени к дате
future_date = today + datetime.timedelta(hours=7)
print(f"Дата через 7 дней: {future_date}")

date_str = input("Введите дату (дд/мм/гггг): ")
my_date = datetime.datetime.strptime(date_str, '%d/%m/%Y').date()

# Форматирование даты и времени
date_str = date(2023, 5, 22)
formatted_date = my_date.strftime("""
%a - Сокращенное название дня недели
%A - Полное название дня недели 
%b - Сокращенное название месяца 
%B - Полное название месяца 
%c - Дата и время
%d - День месяца [01,31] 
%H - Час (24-часовой формат) [00,23] 
%I - Час (12-часовой формат) [01,12] 
%j - День года [001,366] 
%m - Номер месяца [01,12] 
%M - Число минут [00,59] 
%p - До полудня или после (при 12-часовом формате)
%S - Число секунд [00,61] 
%U - Номер недели в году (нулевая неделя начинается с воскресенья) [00,53] 
%w - Номер дня недели [0(Sunday),6] 
%W - Номер недели в году (нулевая неделя начинается с понедельника) [00,53] 
%x - Дата 
%X - Время 
%y - Год без века [00,99] 
%Y - Год с веком
""")

print(f"Форматированная дата и время: {formatted_date}")

